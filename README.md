# php-extended/php-email-provider-yopmail-com
A bridge library that binds the yopmail.com API to the email provider interfaces.

![coverage](https://gitlab.com/php-extended/php-email-provider-yopmail-com/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-email-provider-yopmail-com/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-email-provider-yopmail-com ^8`


## Basic Usage

This library is a bridge from the yopmail.com api library to the provider
interface library.


## License

MIT (See [license file](LICENSE)).
