<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-provider-yopmail-com library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\EmailProvider;

use InvalidArgumentException;
use IteratorIterator;
use LengthException;
use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailMetadata;
use PhpExtended\Email\EmailMetadataInterface;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxList;
use PhpExtended\Parser\ParseThrowable;
use RuntimeException;

/**
 * YopmailComBridgeEmailMetadataIterator class file.
 * 
 * This class is an iterator which transforms metadata from the yopmail api
 * to standardized metadata objects.
 * 
 * @author Anastaszor
 * @extends \IteratorIterator<integer, EmailMetadataInterface, \Iterator<integer, EmailMetadataInterface>>
 */
class YopmailComEmailMetadataIterator extends IteratorIterator
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::current()
	 * @return EmailMetadataInterface
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function current() : EmailMetadataInterface
	{
		/** @var \PhpExtended\ApiComYopmail\ApiComYopmailEmailMetadata $current */
		$current = parent::current();
		
		return new EmailMetadata(
			'yopmail.com',
			(string) $current->getId(),
			(string) $current->getObject(),
			new MailboxList([new Mailbox($current->getAddress())]),
			new EmailAddressList([new EmailAddress('server', new Domain(['yopmail', 'com']))]),
			$current->getDateReception(),
		);
	}
	
}
