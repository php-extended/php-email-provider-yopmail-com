<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-provider-yopmail-com library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\EmailProvider;

use DateTimeImmutable;
use InvalidArgumentException;
use Iterator;
use LengthException;
use PhpExtended\ApiComYopmail\ApiComYopmailEmailMetadata;
use PhpExtended\ApiComYopmail\ApiComYopmailEndpoint;
use PhpExtended\Domain\Domain;
use PhpExtended\Email\Email;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailInterface;
use PhpExtended\Email\EmailMetadataInterface;
use PhpExtended\HttpMessage\StringStream;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;

/**
 * YopmailComBridgeProvider class file.
 * 
 * This class bridges the yopmail api endpoint to be a provider interface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class YopmailComEmailProvider implements EmailProviderInterface
{
	
	/**
	 * The yopmail api endpoint.
	 * 
	 * @var ApiComYopmailEndpoint
	 */
	protected ApiComYopmailEndpoint $_api;
	
	/**
	 * The username of the account to use.
	 * 
	 * @var string
	 */
	protected string $_login;
	
	/**
	 * Builds a new YopmailComBridgeProvider with the given api endpoint.
	 * 
	 * @param ApiComYopmailEndpoint $endpoint
	 * @param string $login
	 */
	public function __construct(ApiComYopmailEndpoint $endpoint, $login)
	{
		$this->_api = $endpoint;
		$this->_login = \trim(\str_replace('@yopmail.com', '', $login));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\EmailProvider\EmailProviderInterface::listEmails()
	 * @return Iterator<int, EmailMetadataInterface>
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function listEmails(int $page = 0) : Iterator
	{
		$emailMetadatas = $this->_api->getEmailMetadatas($this->_login, $page + 1);
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		return new YopmailComEmailMetadataIterator($emailMetadatas);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\EmailProvider\EmailProviderInterface::getEmail()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function getEmail(EmailMetadataInterface $metadata) : EmailInterface
	{
		$yopmailMetadata = new ApiComYopmailEmailMetadata(
			new EmailAddress($this->_login, new Domain(['yopmail', 'com'])),
			$metadata->getIdentifier(),
			$metadata->getSubject(),
			new DateTimeImmutable(),
			'',
		);
		
		$yopmailEmail = $this->_api->getEmail($yopmailMetadata);
		
		$email = new Email();
		$email = $email->withTo(new EmailAddressList([new EmailAddress($this->_login, new Domain(['yopmail', 'com']))]));
		$email = $email->withIdentifier($yopmailEmail->getId());
		$email = $email->withFrom($yopmailEmail->getFrom());
		$email = $email->withReceptionDate($yopmailEmail->getDateReception());
		$email = $email->withSubject($yopmailEmail->getObject());
		/** @var Email $email */
		$email = $email->withBody(new StringStream($yopmailEmail->getContent()));
		
		/** @var Email $email */
		return $email->withProviderName('yopmail.com');
	}
	
}
