<?php declare(strict_types=1);

/**
 * Test script for the YopmailComBridgeProvider class.
 * 
 * Usage :
 * php test.php listEmails <username> <page>?
 * 
 * @author Anastaszor
 */

use PhpExtended\Css\CssSelectorParser;
use PhpExtended\Email\EmailMetadataInterface;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlTransformerFactoryConfiguration;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\HttpClient\ClientFactoryConfiguration;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\ResponseFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\YopmailComApi\YopmailComApiEndpoint;
use PhpExtended\Html\HtmlTransformerFactory;
use PhpExtended\EmailProvider\YopmailComEmailProvider;
use PhpExtended\Email\MailboxParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the method of the endpoint to call.');
}
if(!isset($argv[2]))
{
	throw new InvalidArgumentException('The second argument should be the login to the box to look at.');
}

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$method = trim($argv[1]);
$username = trim($argv[2]);

$clientFactory = new ClientFactory(new ResponseFactory(), new StreamFactory(), new BasicConsoleLogger());
$config = new ClientFactoryConfiguration();
$config->disablePreferCurl();
$client = $clientFactory->createClient($config);
$htmlConfig = new HtmlTransformerFactoryConfiguration();
$htmlConfig->enableScriptFilter()->enableStyleFilter();
$htmlTransf = new HtmlTransformerFactory($htmlConfig);
$endpoint = new YopmailComApiEndpoint($client, new UriFactory(), new RequestFactory(), HtmlParser::get(), $htmlTransf, CssSelectorParser::get());
$bridge = new YopmailComEmailProvider($endpoint, new MailboxParser(), $username);

$rclass = new ReflectionClass($bridge);
$rmethod = $rclass->getMethod($method);
if($rmethod === null)
{
	throw new InvalidArgumentException('The method "'.$method.'" does not exists in the bridge.');
}
$argcount = $rmethod->getNumberOfRequiredParameters();

$args = array();
for($i = 0; $i < max(count($argv), $argcount); $i++)
{
	if(!isset($argv[3+$i]))
	{
		if($i < $argcount)
		{
			throw new InvalidArgumentException('The '.(3+$i).'th argument should not be empty.');
		}
		else
		{
			continue;
		}
	}
	$args[] = $argv[3+$i];
}

$result = call_user_func_array(array($bridge, $method), $args);

var_dump($result);

if($result instanceof \Iterator)
{
	foreach($result as $iterable)
	{
		if($iterable instanceof EmailMetadataInterface)
		{
			$result2 = $bridge->getEmail($iterable);
			var_dump($result2);
		}
	}
}
