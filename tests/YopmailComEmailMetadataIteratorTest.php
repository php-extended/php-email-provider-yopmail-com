<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-provider-yopmail-com library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\EmailProvider\YopmailComEmailMetadataIterator;
use PHPUnit\Framework\TestCase;

/**
 * YopmailComEmailMetadataIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\EmailProvider\YopmailComEmailMetadataIterator
 *
 * @internal
 *
 * @small
 */
class YopmailComEmailMetadataIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var YopmailComEmailMetadataIterator
	 */
	protected YopmailComEmailMetadataIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new YopmailComEmailMetadataIterator(
			new ArrayIterator(),
		);
	}
	
}
