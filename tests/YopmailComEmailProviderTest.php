<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-provider-yopmail-com library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiComYopmail\ApiComYopmailEndpoint;
use PhpExtended\EmailProvider\YopmailComEmailProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

/**
 * YopmailComEmailProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\EmailProvider\YopmailComEmailProvider
 *
 * @internal
 *
 * @small
 */
class YopmailComEmailProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var YopmailComEmailProvider
	 */
	protected YopmailComEmailProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new YopmailComEmailProvider(
			new ApiComYopmailEndpoint(
				$this->getMockForAbstractClass(ClientInterface::class),
			),
			'login',
		);
		
	}
	
}
